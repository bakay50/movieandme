// Components/FilmItem.js

import React from 'react'
import {StyleSheet, View, Text, Image, TouchableOpacity, TouchableWithoutFeedback} from 'react-native'
import {getImageFromApi} from '../API/TMDBApi'
import FadeIn from '../Animations/FadeIn'
import moment from 'moment'
import {connect} from "react-redux";


class FilmItem extends React.Component {

    constructor(props) {
        super(props)
        this._toggleTitle = this._toggleTitle.bind(this)
    }

    _displayFavoriteImage() {
        if (this.props.isFilmFavorite) {
            // Si la props isFilmFavorite vaut true, on affiche le 🖤
            return (
                <Image
                    style={styles.favorite_image}
                    source={require('../Images/ic_favorite.png')}
                />
            )
        }
    }

    _toggleTitle(film) {
        const action = {type: "TOGGLE_TITLE", value: film}
        this.props.dispatch(action)
    }

    _updateTitleButton(film) {
        var currentTitle = film.title
        if (this.props.historicTitle.length > 0) {
            const filmIndex = this.props.historicTitle.findIndex(item => item.id === film.id)
            if (filmIndex !== -1) {
                currentTitle = 'Sorti le '.concat(moment(new Date(film.release_date)).format('DD/MM/YYYY'))
            }
        }
        return (
            <Text style={styles.title_text_vu}>{currentTitle}</Text>
        )
    }


    render() {
        const {film, displayDetailForFilm, filmsVuList} = this.props
        if (filmsVuList) {
            return (
                <View style={styles.main_container_vu}>
                    <TouchableOpacity onPress={() => displayDetailForFilm(film.id)}>
                        <Image
                            style={styles.image_vu} resizeMode="cover"
                            source={{uri: getImageFromApi(film.poster_path)}}/>
                    </TouchableOpacity>

                    <TouchableWithoutFeedback delayLongPress={3800} onPress={() => displayDetailForFilm(film.id)}
                                              onLongPress={() => this._toggleTitle(film)}>

                        <View style={styles.header_container_vu}>
                            {this._updateTitleButton(film)}
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            )
        } else {
            return (
                <FadeIn>
                    <TouchableOpacity
                        style={styles.main_container}
                        onPress={() => displayDetailForFilm(film.id)}>
                        <Image
                            style={styles.image}
                            source={{uri: getImageFromApi(film.poster_path)}}
                        />
                        <View style={styles.content_container}>
                            <View style={styles.header_container}>
                                {this._displayFavoriteImage()}
                                <Text style={styles.title_text}>{film.title}</Text>
                                <Text style={styles.vote_text}>{film.vote_average}</Text>
                            </View>
                            <View style={styles.description_container}>
                                <Text style={styles.description_text} numberOfLines={6}>{film.overview}</Text>
                            </View>
                            <View style={styles.date_container}>
                                <Text style={styles.date_text}>Sorti
                                    le {moment(new Date(film.release_date)).format('DD/MM/YYYY')}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </FadeIn>
            )
        }
    }
}

const styles = StyleSheet.create({
    main_container: {
        height: 190,
        flexDirection: 'row'
    },
    image: {
        width: 120,
        height: 180,
        margin: 5
    },
    content_container: {
        flex: 1,
        margin: 5
    },
    header_container: {
        flex: 3,
        flexDirection: 'row'
    },
    title_text: {
        fontWeight: 'bold',
        fontSize: 20,
        flex: 1,
        flexWrap: 'wrap',
        paddingRight: 5
    },
    vote_text: {
        fontWeight: 'bold',
        fontSize: 26,
        color: '#666666'
    },
    description_container: {
        flex: 7
    },
    description_text: {
        fontStyle: 'italic',
        color: '#666666'
    },
    date_container: {
        flex: 1
    },
    date_text: {
        textAlign: 'right',
        fontSize: 14
    },
    favorite_image: {
        width: 25,
        height: 25,
        marginRight: 5
    },
    main_container_vu: {
        flex: 1,
        flexDirection: 'row',
        marginHorizontal: 5,
        marginVertical: 1
    },
    image_vu: {
        paddingTop: 11,
        marginTop: 11,
        paddingBottom: 5,
        borderWidth: 1,
        width: 98,
        height: 98,
        alignSelf: 'center',
        borderRadius: 75,
        flexDirection: 'row'
    },
    header_container_vu: {
        textAlign: 'left',
        width: 350,
        height: 100,
        margin: 8,
        flexDirection: 'row'
    },
    title_text_vu: {
        flex: 2,
        paddingTop: 29,
        alignItems: 'center',
        paddingLeft: 6,
        paddingRight: 6
    }
})

const mapStateToProps = (state) => {
    return {
        historicFilms: state.toggleFilmVu.historicFilms,
        historicTitle: state.toggleTitle.historicTitle
    }
}

export default connect(mapStateToProps)(FilmItem)

