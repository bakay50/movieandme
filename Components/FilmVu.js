// Components/FilmVu.js

import React from 'react'
import {StyleSheet, Text, View} from 'react-native'
import FilmList from './FilmList'
import {connect} from 'react-redux'


class FilmVu extends React.Component {

    render() {
        return (
            <View style={styles.main_container}>
                <FilmList
                    films={this.props.historicFilms}
                    navigation={this.props.navigation}
                    filmsVuList={true}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1
    }
})

const mapStateToProps = (state) => {
    return {
        historicFilms: state.toggleFilmVu.historicFilms
    }
}

export default connect(mapStateToProps)(FilmVu)