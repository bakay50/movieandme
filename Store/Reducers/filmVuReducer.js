// Store/Reducers/filmVuReducer.js

const initialState = { historicFilms: [] }

function toggleFilmVu(state = initialState, action) {
    let nextState
    switch (action.type) {
        case 'TOGGLE_FILM_VU':
            const filmVuIndex = state.historicFilms.findIndex(item => item.id === action.value.id)
            if (filmVuIndex !== -1) {
                nextState = {
                    ...state,
                    historicFilms: state.historicFilms.filter( (item, index) => index !== filmVuIndex)
                }
            }
            else {
                nextState = {
                    ...state,
                    historicFilms: [...state.historicFilms, action.value]
                }
            }
            return nextState || state
        default:
            return state
    }
}

export default toggleFilmVu