// Store/Reducers/titleReducer.js

const initialState = { historicTitle: []}

function toggleTitle(state = initialState, action) {
    let nextState
    switch (action.type) {
        case 'TOGGLE_TITLE':
            const filmVuIndex = state.historicTitle.findIndex(item => item.id === action.value.id)
            if (filmVuIndex !== -1) {
                nextState = {
                    ...state,
                    historicTitle: state.historicTitle.filter( (item, index) => index !== filmVuIndex)
                }
            }else{
                nextState = {
                    ...state,
                    historicTitle: [...state.historicTitle, action.value]
                }
            }
            return nextState || state
        default:
            return state
    }
}

export default toggleTitle